defmodule Zeiter.Repo do
  use Ecto.Repo,
    otp_app: :zeiter,
    adapter: Ecto.Adapters.Postgres
end
