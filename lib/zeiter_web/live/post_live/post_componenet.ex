defmodule ZeiterWeb.PostLive.PostComponenet do
  use ZeiterWeb, :live_component

  def render(assigns) do
    ~L"""
    <div id="post-<%= @post.id %>" class="post">
      <div class="row">
        <div class="colum column-10">
          <div class="post-avatar">
            <img src="https://via.placeholder.com/100" alt="<%= @post.username %>">
          </div>
        </div>
        <div class="column column-90 post-body">
          <b>@<%= @post.username %></b>
          <br>
          <%= @post.body %>
        </div>
      </div>

      <div class="row">
        <div class="column">
        <a href="#" phx-click="like" phx-target="<%= @myself %>">
          <span>💟</span></i> <%= @post.likes_count %>
        </a>
        </div>
        <div class="column">
        <a href="#" phx-click="repost" phx-target="<%= @myself %>">
          <span>🔄</span></i> <%= @post.reposts_count %>
        </a>
        </div>
        <div class="column">
          <%= live_patch to: Routes.post_index_path(@socket, :edit, @post.id) do %>
          <span>✏️</span>
          <% end %>
          <%= link to: "#", phx_click: "delete", phx_value_id: @post.id, data: [confirm: "'r you sure?"] do %>
          <span>❌</span>
          <% end %>
        </div>
      </div>
    </div>
    """
  end

  def handle_event("like", _, socket) do
    Zeiter.Wall.inc_likes(socket.assigns.post)
    {:noreplay, socket}
  end

  def handle_event("repost", _, socket) do
    Zeiter.Wall.inc_reposts(socket.assigns.post)
    {:noreplay, socket}
  end
end
