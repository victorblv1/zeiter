# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :zeiter,
  ecto_repos: [Zeiter.Repo]

# Configures the endpoint
config :zeiter, ZeiterWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ypTk6iHO+Is55i+hT5E1R/ACDiUIV6E4I2ovLdHVU+Ark3Wrrk/9ewFlxe6Dbnek",
  render_errors: [view: ZeiterWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Zeiter.PubSub,
  live_view: [signing_salt: "3htB8fBb"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
