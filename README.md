# Zeiter

Before starting Phoenix server:

  * Make sure you're started your PostgreSQL server

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

To get to the Wall and get access to the posts:

  * go to the following link: [`localhost:4000/posts`](http://localhost:4000/posts) in your browser

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

To finish the session:

  * Stop Phoenix server with the 'ctrl-C' in your command line, and enter `a` for `abort` command
  * Stop PostgreSQL server